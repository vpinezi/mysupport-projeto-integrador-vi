# mySupport - Projeto Integrador VI



## Descrição

Aplicativo para abertura e gerenciamento de chamados de suporte técnico criado como solução de um problema para uma empresa de software. Projeto desenvolvido para o a disciplina de Projeto Integrador do curso de Engenharia da Computação da Universidade Virtual do Estado de São Paulo (UNIVESP). Integrantes do grupo: Luis Jamelli, Vinícius Pinezi e Wilson Tamashiro.

## Técnologia

Aplicativo desenvolvido utilizando o React Native, WebService baseado em .NetCore e banco de dados SQL Server.

## Roadmap

- [X] Estrutura inicial do aplicativo;
- [ ] Melhorias de novas funcionalidades principais;
- [ ] Refatorização do aplicativo;
- [ ] WebService em .NETCore;
- [ ] Integração com software para automatização das aberturas;

## Installation
Em breve.

## License
MIT license.

## Project status
On going;
